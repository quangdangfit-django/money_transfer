from django.contrib.auth.models import User
from django.db import models


# Create your models here.
class Transaction(models.Model):
    account_number = models.BigAutoField(null=False, blank=True)
    uid = models.ForeignKey(User, db_column='uid', on_delete=models.CASCADE, null=False, blank=True)
    balance = models.IntegerField(default=0, null=True, blank=True)
    create_at = models.DateTimeField(auto_now=True, null=True, blank=True)

    class Meta:
        db_table = 'transactions'
